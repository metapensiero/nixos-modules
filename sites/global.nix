# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- sites configuration
# :Created:   mar 30 ott 2018 17:48:36 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

{config, lib, options, ...}:
  let
    inherit (lib) attrValues concatStringsSep flip mapAttrs mapAttrsToList
      mkIf mkMerge versionAtLeast;
    inherit (config.metapensiero.lib.nginx) mkRedirects mkQueryRedirects;
    sep = "\n";
    mkLogging = logsBase: vh: ''
       error_log ${logsBase}/${vh.serverName}-error.log;
       access_log ${logsBase}/${vh.serverName}-access.log combined;
    '';
    vHost = sitesCfg: site: siteVH:
      siteVH // { extraConfig = concatStringsSep sep [
                    (if siteVH ? "extraConfig"
                      then siteVH.extraConfig else "")
                    (mkRedirects site.redirects)
                    (mkQueryRedirects site.queryRedirects)
                    (mkLogging sitesCfg.logsBase siteVH)
                  ];
      };
    cfg = config.metapensiero.sites;
    # WARN: generates the same redirects for all the vhosts!
    siteVHosts = sitesCfg: site: flip mapAttrs site.virtualHosts
      (name: vh: vHost sitesCfg site vh);
  in {
    config = mkIf (builtins.length (attrValues cfg.sites) > 0) {
      services.nginx = {
        enable =  true;
        virtualHosts = mkMerge (map (site: siteVHosts cfg site)
          (attrValues cfg.sites));
      };
      services.logrotate =
        let
          logConfig = user: group: ''
            weekly
            rotate 8

            ${cfg.logsBase}/*.log {
              weekly
              missingok
              rotate ${builtins.toString cfg.logsRetentionWeeks}
              compress
              delaycompress
              notifempty
              create 0640 ${user} ${group}
              sharedscripts
              postrotate
                systemctl kill -s USR1 nginx
              endscript
            }
          '';
          systemVersion = config.system.nixos.release;
          cfgAttrName = if (versionAtLeast systemVersion "20.09")
                        then "extraConfig" else "config";
        in {
            enable = true;
            "${cfgAttrName}" = logConfig config.services.nginx.user config.services.nginx.group;
        };
      system.activationScripts.sitesLogging = ''
        mkdir -p ${cfg.logsBase}
        chown ${config.services.nginx.user}.${config.services.nginx.group} ${cfg.logsBase}
      '';
      containers = mkMerge (map (site: site.containers) (attrValues cfg.sites));
    };
  }
