# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- sites backup
# :Created:   mar 29 gen 2019 18:06:26 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2019 Alberto Berti
#

{ pkgs, config, lib, ... }:
  let
    inherit (lib) filterAttrs length mapAttrsToList mkIf mkMerge optionalString;
    bckCfg = config.metapensiero.backup;
    sitesCfg = config.metapensiero.sites.sites;
    calcRepoName = site: bckCfg.mountDir + "/sites/${site.siteName}";
    /* For a given site (supposedly already filtered to have something
       to backup) build a backup definition conforming to
       "services.borgbackup.jobs"
    */
    backupJobForSite = site: {
      archiveBaseName = "${config.networking.hostName}-site-${site.siteName}";
      compression = "zstd";
      doInit = true;
      encryption = {
        # see https://borgbackup.readthedocs.io/en/stable/usage/init.html#encryption-modes
        mode = "repokey-blake2";
        passCommand = ''
          cat ${bckCfg.keyFile}
        '';
      };
      extraCreateArgs = "--stats";
      paths = site.backupDirs;
      postHook = ''
        ${site.backupPostScript}
      '';
      preHook = ''
        echo "Borg repo is: $BORG_REPO"
        ${site.backupPreScript}
        ${optionalString (site.additionalBackupPre != null)
          site.additionalBackupPre}
        export BORG_RELOCATED_REPO_ACCESS_IS_OK="yes"
      '';
      prune = {
        keep = {
          within = "1d"; # Keep all archives from the last day
          daily = 7;
          weekly = 4;
          monthly = -1;  # Keep at least one archive for each month
        };
      };
      repo = calcRepoName site;
      startAt = "daily";
    };
    borgJobNameFor = site: "borgbackup-job-${site.siteName}";
    borgJobServiceNameFor = site: "${borgJobNameFor site}.service";
    backupPrepareJobForSite = site: {
      description = "Backup prepare job for site ${site.siteName}";
      requiredBy = [ (borgJobServiceNameFor site) ];
      before = [ (borgJobServiceNameFor site) ];
      script = ''
        mkdir -p ${calcRepoName site}
      '';
      serviceConfig = {
        Type = "oneshot";
      };
      unitConfig = {
        RequiresMountsFor = "${bckCfg.mountDir}";
      };
      # wantedBy = [
      #   "multi-user.target"
      # ];
    };
    activeBackups = filterAttrs (n: site: (length site.backupDirs) > 0) sitesCfg;
  in {
    # for every site (in sitesCfg) that has something to backup build
    # a set with one only attr with the same name and a backup
    # definition as value. These will be later merged by mkMerge by
    # the modules dependency resolution logic. I could also have used
    # "foldAttrs" instead of mkMerge
    config.services.borgbackup.jobs = mkMerge (mapAttrsToList
      (n: site: (mkIf bckCfg.enable { ${site.siteName} = backupJobForSite site;}))
        (activeBackups));
    config.metapensiero.backup.mountRequiredBy = mkIf bckCfg.enable
      (mapAttrsToList (n: site: borgJobServiceNameFor site)
        (activeBackups));
    config.systemd.services = mkMerge (mapAttrsToList
      (n: site: (mkIf bckCfg.enable
        { "metapensiero-backup-prepare-${site.siteName}" = backupPrepareJobForSite site;}))
        (activeBackups));
    config.systemd.timers = mkMerge (mapAttrsToList
      (n: site: (mkIf bckCfg.enable
        { ${borgJobNameFor site} = {
            timerConfig = {
              RandomizedDelaySec = "2h";
            };
          };
        }))
        (activeBackups));
  }
