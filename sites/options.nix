{config, pkgs, lib, ...}:
  with lib;
  let
    domainsSubmodule = {
      options = {
        main = mkOption {
          description = ''
            Main domain of the website
          '';
          type = types.str;
        };
        aliases = mkOption {
          description = ''
            aliases domains
          '';
          default = [];
          type = with types; listOf str;
        };
      };
    };
    queryRedirectSubmodule = {config, name, ...}: {
      options = {
        location = mkOption {
          description = ''
            The location which is the base of the query matchings
          '';
          type = types.str;
          default = name;
        };
        queryMappings = mkOption {
          description = ''
            A set containing mappings like "query_string = relative_or_full_URL"
          '';
          default = {};
          type = types.attrs;
        };
      };
    };
    globalConfig = config;
    sitesSubmodule = {config, name, ...}:{
      options = {
        siteName = mkOption {
          description = ''
            The name of the site. If unspecified, the attribute
            name will be used.
          '';
          type = types.nullOr types.str;
          default = null;
          example = "uncliente";
        };
        domains = mkOption {
          description = ''
            Information on the domains connected with the site
          '';
          type = types.submodule domainsSubmodule;
        };
        pkg = mkOption {
          description = ''
            Package of the site
          '';
          type = with types; package;
        };
        enableSSL = mkOption {
          description = ''
            Enable SSL certificate requests
          '';
          default = false;
          type = with types; types.bool;
        };
        uwsgiSocket = mkOption {
          description = ''
            Socket where the uwsgi process will be listening
          '';
          example = "localhost:9000";
          type = with types; str;
        };
        environmentName = mkOption {
          description = ''
            Environment (or "settings") name for the site
          '';
          default = "production";
          type = with types; str;
        };
        siteType = mkOption {
          description = ''
            Type of the site, for now it can only be "wagtail"
          '';
          default = "custom";
          type = with types; nullOr (enum [ "custom" ]);
        };
        policy = mkOption {
          description = ''
            Policy about site home maintenance:
            none
              do nothing
            reset
              erase and reinit the home dir at each boot
            upgrade
              run the upgrade script at each (re)-boot
          '';
          default = "upgrade";
          type = with types; enum [
            "none"
            "reset"
            "upgrade"
          ];
        };
        mountDir = mkOption {
          description = ''
            The optional path of a directory to bind mount into the site home
          '';
          default = null;
          type = with types; nullOr path;
        };
        disableHostnameFilter = mkOption {
          description = ''
            Set DJANGO_ALLOWED_HOSTS=* thus enablig the site to be used as ``default``,
            regardless of HTTP's ``Host`` header used to access it.
            Useful when doing local debugging.
          '';
          default = false;
          type = with types; bool;
        };
        containers = mkOption {
          description = ''
            An attr containing containers, will be merged with the main
            ``containers`` attribute.
          '';
          default = {};
          type = with types; attrs;
        };
        virtualHosts = mkOption {
          description = ''
            An attr containing virtual hosts, will be merged with the main
            ``services.nginx.virtualHosts`` attribute.
          '';
          default = {};
          type = with types; attrs;
        };
        timeZone = mkOption {
          description = ''
            The timezone of the services created
          '';
          default = "UTC";
          type = with types; str;
        };
        backupDirs = mkOption {
          description = ''
            Directories to backup
          '';
          default = [];
          type = with types; listOf path;
        };
        backupPreScript = mkOption {
          description = ''
            The script to execute before the backup.
          '';
          default = null;
          type = with types; nullOr str;
        };
        backupPostScript = mkOption {
          description = ''
            The script to execute after the backup.
          '';
          default = null;
          type = with types; nullOr str;
        };
        additionalBackupPre = mkOption {
          description = ''
            Additional bash lines to be added to the backup "preHook".
            Used especially for testing purposes.
          '';
          default = null;
          type = with types; nullOr str;
        };
        redirects = mkOption {
          description = ''
            A set containing mappings like "regexp = relative_or_full_URL" to
            resolve non-existent pages. It will generate permanent redirects (301).
          '';
          default = {};
          type = with types; attrs;
        };
        queryRedirects = mkOption {
          description = ''
            An attrs whose keys are location paths (literal, not regexp) and the
            values are attrs with a queryMappings entry which in turn is a an attrs
            that maps query strings to destinations. The query string is supposed
            to be a regexp but without captures, see the example.
            It will generate permanent redirects (301).

            The query string wil be matched "as is", so the order of its components
            matters and is not supposed to change.

            Be aware that this will generate locations that may clash with those
            explicitly created by you.
          '';
          default = {};
          type = types.attrsOf (types.submodule queryRedirectSubmodule);
          example = literalExample ''
          {
            "/index.php" = {
              queryMappings = {
                "name=foo&foo=name" = "http://localhost:3000";
                "foo=.*" = "/pippo
              };
            };
          };
          '';

        };
        additionalEnv = mkOption {
          description = ''
            An attr containing additional environment variables to set in the site.
          '';
          default = {};
          type = with types; attrs;
        };
        mailDomain = mkOption {
          description = ''
            The domain part of "From:" mail addresses
          '';
          default = globalConfig.networking.domain;
          type = types.str;
        };
        pkgs = mkOption {
          type = types.nullOr types.attrs;
          default = null;
          example = literalExample "pkgs";
          description = ''
            Customise which nixpkgs to use for the container(s) generated by
            this site.
          '';
        };
      };
      config.siteName = mkDefault name;
    };
  in {
    options.metapensiero = {
      sites = {
         logsBase = mkOption {
          description = ''
            The directory where logging files will be placed
          '';
          default = "/var/log/nginx";
          example = "/var/log/nginx";
          type = types.str;
        };
        logsRetentionWeeks = mkOption {
          description = ''
            How many weeks of logging files to keep
          '';
          default = 52;
          example = 52;
          type = types.int;
        };
        sites = mkOption {
          description = ''
            Sites running on this system
          '';
          default = {};
          type = with types; attrsOf (submodule sitesSubmodule);
        };
      };
    };
  }
