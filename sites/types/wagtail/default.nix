{pkgs, config, lib, ...}:
  let
    inherit (lib) concatStringsSep mapAttrsToList mkIf mkOption optionalAttrs
      toUpper types;
    sitesSubmodule = {config, name, ...}:
      let
        siteName = config.siteName;
        siteHome = "/home/${siteName}";
        siteRoot = if config.mountDir == null
          then "/var/lib/containers/${config.siteName}${siteHome}"
          else config.mountDir;
        allowedSites = concatStringsSep " " (
          [config.domains.main] ++ config.domains.aliases);
        runEnv = {
          "${toUpper sitePkg.name}_HOME" = siteHome;
          SITE_HOME = siteHome;
          CURRENT_VERSION = sitePkg.version;
          # The DJANGO_* variables are defined inside the wagtail's
          # (pirlo?) production profile
          DJANGO_ALLOWED_HOSTS = "${if config.disableHostnameFilter
                                      then "*"
                                      else allowedSites}";
          DJANGO_BASE_URL = "https://${config.domains.main}";
          DJANGO_SETTINGS_MODULE = "${sitePkg.name}.settings.${config.environmentName}";
          DJANGO_SITE_FROM_EMAIL = "${siteName}-noreply@${config.mailDomain}";
          DJANGO_TIME_ZONE = "${config.timeZone}";
          INIT_SCRIPT = "${sitePkg}/bin/${sitePkg.name}-init-home";
          LANG = "en_US.UTF-8";
          MANAGE_SCRIPT = "${sitePkg}/bin/${sitePkg.name}-manage";
          POLICY = config.policy;
          PYTHONUNBUFFERED = "1";
        } // config.additionalEnv;
        completeManageScript = pkgs.writeShellScriptBin "${sitePkg.name}-manage" ''
          ${concatStringsSep "\n" (
             mapAttrsToList (
               name: value: "export ${name}=\"${value}\""
             )
             runEnv)
          }
          exec $MANAGE_SCRIPT $@
        '';
        usedPkgs = if config.pkgs == null
                   then pkgs
                   else config.pkgs;
        util = import ../util.nix {
          # this is where the arguments to the "submodules" (like
          # "virtualhosts.nix", those called with "callModule")
          # loading function gets added
          pkgs = usedPkgs;
          inherit siteName siteHome siteRoot runEnv;
          inherit (usedPkgs) lib;
          site = config;
        };
        inherit (util) sitePkg callModule;
        containersCtlScript = pkgs.nixos-container + /bin/nixos-container;
      in {
        options.siteType = mkOption {
          type = with types; nullOr (enum [ "wagtail" ]);
        };
        config =  mkIf (config.siteType == "wagtail") {
          containers.${siteName} = {
            autoStart = true;
            bindMounts = if config.mountDir != null then {
              "${siteHome}" = {
                  hostPath = config.mountDir;
                  isReadOnly = false;
                };
            } else {};
            config = {
              environment.systemPackages = [
                completeManageScript
                sitePkg
                pkgs.sqlite
              ];
              users.users."${siteName}" = {
                isNormalUser = true;
              };
              services.uwsgi = callModule ./uwsgi.nix;
              systemd = callModule ./systemd.nix;
              time.timeZone = config.timeZone;
            };
          } // (optionalAttrs (config.pkgs != null) {
            pkgs = config.pkgs;
          });
          virtualHosts.${siteName} = callModule ./virtualhost.nix;
          backupDirs = [ siteRoot ];
          backupPreScript = ''
            ${containersCtlScript} stop ${siteName} \
              && echo "Stopped Wagtail site ${siteName}"
          '';
          backupPostScript = ''
            ${containersCtlScript} start ${siteName} \
              && echo "Started Wagtail site ${siteName}"
          '';
        };
      };
  in {
    options.metapensiero.sites.sites = mkOption {
      type = with types; attrsOf (submodule sitesSubmodule);
    };
  }
