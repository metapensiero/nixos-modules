{ lib, site, sitePkg, siteHome, runEnv, ...}:
  with lib;
  {
    enable = true;
    user = site.siteName;
    group = "users";
    plugins = ["python3"];
    instance = {
      type = "normal";
      master = true;
      pythonPackages = self: sitePkg.pythonDeps;
      module = "${sitePkg.name}.wsgi:application";
      chdir = "${sitePkg}";
      socket = site.uwsgiSocket;
      processes = 50;
      cheaper = 10;
      vacuum = true;
      max-requests = 5000;
      # the following value is to prevent connection errors from
      # nginx (the default is 128) see also
      # boot.kernel.sysctl."net.core.somaxconn" and:
      # http://man7.org/linux/man-pages/man2/listen.2.html
      # https://stackoverflow.com/questions/44581719/resource-temporarily-unavailable-using-uwsgi-nginx
      listen = 1024;
      env = mapAttrsToList (name: value: "${name}=${value}") runEnv;
    };
  }
