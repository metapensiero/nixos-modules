{pkgs, lib, site, sitePkg, siteHome, runEnv, ...}:
  with lib;
  let
    baseServiceConfig = {
        enable = true;
        environment = runEnv;
        path = [ sitePkg pkgs.gawk ];
        serviceConfig = {
            Group = "users";
            Type = "oneshot";
            User = site.siteName;
            WorkingDirectory = "~";
        };
      };
  in {
    services = {
      manage_home = baseServiceConfig // {
        before = [ "uwsgi.service" ];
        requiredBy = [ "uwsgi.service" ];
        description="Handles the initialization and the maintenance of the website home directory.";
        wantedBy = [
          "multi-user.target"
        ];
        script = (readFile ./wagtail-site-activation.sh);
      };
      publish_content = baseServiceConfig // {
        description = "Publish wagtail content";
        script = ''
          $MANAGE_SCRIPT publish_scheduled_pages
          echo "Publish script run"
        '';
      };
    };
    timers = {
      publish_content = {
        after = [ "uwsgi.service" ];
        bindsTo = [ "uwsgi.service" ];
        enable = true;
        requiredBy = [ "uwsgi.service" ];
        timerConfig = {
          RandomizedDelaySec = "1min";
          # See also the command "systemd-analize calendar <spec>"
          OnCalendar = "*:0/10";  # every ten min.
          Persistent = true;
        };
        wantedBy = [ "timers.target" ];
      };
    };
  }
