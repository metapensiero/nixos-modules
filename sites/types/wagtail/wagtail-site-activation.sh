# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- site home dir manage script
# :Created:   mer 31 ott 2018 17:00:23 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#
echo "Runing home manage script..."
echo "Policy is \"$POLICY\""

LAST_VERSION=$(cat .last_version || printf "0")

echo "Last version is \"$LAST_VERSION\""
echo "Current version is \"$CURRENT_VERSION\""

# set the home world readable to let nginx serve the files
chmod go+rx .

if [[ $POLICY == "reset" ]]; then
    rm -rf *
    echo "Data erased."
fi

if [[ -z $(ls) ]]; then
    $INIT_SCRIPT
    $MANAGE_SCRIPT collectstatic --no-input -c
    echo -n $CURRENT_VERSION > .last_version
    echo "Data initialized"
fi

function version {
    echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }';
}

if [[ ($POLICY == "upgrade") && \
          (( $(version $CURRENT_VERSION) > $(version ${LAST_VERSION:-0}) )) ]]; then
    $MANAGE_SCRIPT migrate --no-input
    $MANAGE_SCRIPT collectstatic --no-input -c
    echo -n $CURRENT_VERSION > .last_version
    echo "Data upgraded"
fi
