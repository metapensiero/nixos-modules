{pkgs, lib, site, siteRoot, ... }:
  with lib;
  let
  in {
    enableACME = false;
    serverName = site.domains.main;
    serverAliases = site.domains.aliases;
    locations = {
      "/media/" = {
        priority = 500;
        alias = "${siteRoot}/media/";
      };
      "/static/" = {
        priority = 500;
        alias = "${siteRoot}/static/";
      };
      "/" = {
        extraConfig = ''
          uwsgi_pass ${site.uwsgiSocket};
        '';
      };
    };
  } // (optionalAttrs site.enableSSL {
    forceSSL = true;
    enableACME = true;
  })
