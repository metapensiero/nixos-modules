{pkgs, site, ...}@args: rec {
  # FIXME: I added the second check on ``pythonDeps`` as last
  # resort as I don't understand why the first is true when
  # ``site.pkg`` is fetched instead of referencing a local path
  sitePkg = if (pkgs.lib.isDerivation site.pkg && site.pkg ? pythonDeps)
    then site.pkg
    else import site.pkg {
      inherit pkgs;
      python = pkgs.uwsgi.python3;
    };
    callModule = path: import path (args // { inherit sitePkg;});
  }
