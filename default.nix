# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- configuration modules
# :Created:   mar 30 ott 2018 17:24:46 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Alberto Berti
#

{config, pkgs, lib, ...}: {
  imports = [
    ./backup.nix
    ./sites
    ./analytics
    ./global.nix
  ];
}
