# -*- coding: utf-8 -*-
# :Project:   nixos-modules -- nginx utils
# :Created:   ven 15 gen 2021, 01:36:17
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2021 Alberto Berti
#

/*
A set of functions to help with configuring nginx virtualhosts.

*/

{ config, lib, pkgs, ... }:
  let
    inherit (lib) concatStringsSep mapAttrsToList;
  in {
    /* Define an nginx redirection from a path with multiple paths to destination.

       Example:
         redirs = { "/a/path" = {
             "foo=bar&bar=foo" = "/dest/path";
             "bar=baz" = http://example.com;
           };
         };
         mkQueryRedirects redirs;
         => location /a/path {
              if ($args ~ foo=bar&bar=foo) {
                return 301 /dest/path;
              }
              if ($args ~ bar=baz) {
                return 301 http://example.com;
              }
            }

    The following function doesn't use a much simpler approach with the `map`
    directive of nginx because that needs a "stream" context that requires a
    reconfiguration of all the server (i.e. instead of having an external `http`
    directive a `stream` one has to be used).
    */
    mkQueryRedirects = queryRedirs:
      let
        mkArgsIf = query: dest: ''
          if ($args ~ ${query}) {
            return 301 ${dest};
          }
        '';
        mkRedirLocation = path: redirMaps: ''
          location ${path} {
              ${concatStringsSep "\n" (mapAttrsToList
                (from: to: mkArgsIf from to) redirMaps)}
          }
        '';
      in
        concatStringsSep "\n" (mapAttrsToList
          (loc: redirData: mkRedirLocation loc redirData.queryMappings) queryRedirs);

    /* Create a simple redirect definition given a mapping with
       { match = destination;} entries. Match can be a regexp and destination
       can be a path or an url.
    */
    mkRedirects = redirects:
        concatStringsSep "\n" (mapAttrsToList
          (old: new: "rewrite ${old} ${new} permanent;") redirects);
}
