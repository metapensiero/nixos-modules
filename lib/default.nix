# -*- coding: utf-8 -*-
# :Project:   nixos-modules -- Library entrypoint
# :Created:   ven 15 gen 2021, 01:57:03
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2021 Alberto Berti
#

{ config, lib, pkgs, ... }: {
  nginx = import ./nginx.nix {inherit config lib pkgs;};
}
