# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules --
# :Created:   gio 16 mag 2019 19:30:20 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2019 Alberto Berti
#

/*
  Here are some must know points to work with logstash
  configurations:

  - the code in "code" fragments is Ruby, the other is not;
  - yes the configuration fragments allow for conditional to be
    inserted. Every condition will be evaluated for each event (log
    line, more or less);
  - the documentation for journalbeat is at:
      https://www.elastic.co/guide/en/beats/journalbeat/6.7/logstash-output.html
  - the documentation for logstash is at:
      https://www.elastic.co/guide/en/logstash/current/plugins-filters-aggregate.html
  - to access subfields usually use strings like "[foo][bar]"
  - the "event" object exposed through Ruby is underdocumented, its
    code is here: https://github.com/elastic/logstash/blob/ee2c8af8c8b0a05772813a60cc4f33e56e67b1cc/logstash-core-event/lib/logstash/event.rb
  Also see
    https://www.elastic.co/guide/en/logstash/7.0/plugins-filters-ruby.html
    and
    https://www.elastic.co/guide/en/logstash/7.0/field-references-deepdive.html

*/

{config, pkgs, lib, ...}:
  with lib;
  let
    postfixFilterConf = import ./postfix_filter_conf.nix;
    postfixFilter = ''
      ${postfixFilterConf}
      # if the postfix_queueid field is missing, drop the message altogether.
      if ! [postfix_queueid] {
        drop { }
      }
      if ([program] == "postfix/qmgr") and [postfix_from] {
        aggregate {
          task_id => "%{postfix_queueid}"
          map_action => "create"
          timeout => 600
          code => "
            map['queueid'] = event.get('postfix_queueid')
            map['size'] = event.get('postfix_size')
            map['from'] = event.get('postfix_from')
            map['nrcpt'] = event.get('postfix_nrcpt')
            map['relay_ip'] = 'none'
            map['relay_hostname'] = 'none'
            map['response'] = 'none'
          "
        }
      } else if [program] == "postfix/smtp" {
        aggregate {
          task_id => "%{postfix_queueid}"
          map_action => "update"
          code => "
            data = map.merge({
              'to' => event.get('postfix_to'),
              'delay' => event.get('postfix_delay'),
              'relay_ip' => event.get('postfix_relay_ip'),
              'relay_hostname' => event.get('postfix_relay_hostname'),
              'status' => event.get('postfix_status'),
              'response' => event.get('postfix_smtp_response')
            })
            event.to_hash.each do |key, value|
              if (key =~ /^postfix/) == 0
                event.remove(key)
              end
            end
            event.set('postfix', data)
            event.set('program', 'postfix_aggregated')
          "
        }
      }
    '';
    postfixOut = influxdbHost: DBName: ''
      if [program] == "postfix_aggregated" {
        # file {
        #   path => "/tmp/logstash.txt"
        # }
        # The following cannot be used due to the output module being
        # missing from the standard distribution
        # influxdb {
        #   host => ${influxdbHost}
        #   db => "postfix"
        #   data_points => "%{postfix}"
        #   send_as_tags => ["from", "to", "relay_ip", "relay_hostname", "status"]
        #   measurement => "%{host}"
        # }
        http {
          url => "http://${influxdbHost}:8086/write?db=${DBName}"
          http_method => "post"
          format => "message"
          message => '%{[beat][hostname]},from=%{[postfix][from]},to=%{[postfix][to]},relay_ip=%{[postfix][relay_ip]},relay_hostname=%{[postfix][relay_hostname]},status=%{[postfix][status]} size=%{[postfix][size]},queueid="%{[postfix][queueid]}",nrcpt=%{[postfix][nrcpt]},delay=%{[postfix][delay]},response="%{[postfix][response]}"'
        }
      }
    '';
    getDefaultDomain = definition: if definition != null
      then definition
      else "${config.networking.hostName}.${config.networking.domain}";
    getDefaultRoot = definition: if definition != null
      then definition
      else "http://${getDefaultDomain null}/stats/";
    cfg = config.metapensiero.analytics;
  in {
    imports = [];
    options.metapensiero.analytics = {
      publishLogs = {
        enable = mkEnableOption "log publishing";
        destination = mkOption {
          description = "Where to send the logs";
          default = "localhost";
          type = types.str;
        };
      };
      process = {
        enable = mkEnableOption "analytics processor";
        postfix = {
          enable = mkEnableOption "Postfix specific processing";
          storeDestination = mkOption {
            description = "Where to store the aggregated data (InfluxDB)";
            default = "localhost";
            type = types.str;
          };
          storeDBName = mkOption {
            description = "Name of the InfluxDB database to feed";
            default = "postfix";
            type = types.str;
          };
        };
      };
      store = {
        enable = mkEnableOption "InfluxDB analytics storage";
      };
      show = {
        enable = mkEnableOption "Grafana dashboards";
        domain = mkOption {
          description = "The domain of the dashboards";
          default = null;
          type = with types; nullOr str;
        };
        rootURL = mkOption {
          description = "The root URL of the dashboards";
          default = null;
          type = with types; nullOr str;
        };
      };
    };
    config = {
      nixpkgs.config.allowUnfree = cfg.process.enable;
      services = {
        journalbeat = mkIf cfg.publishLogs.enable {
          enable = true;
          # package = pkgs.journalbeat7;
          extraConfig = ''
            journalbeat.inputs:
              - paths: []
                seek: cursor
                include_matches:
                  - "systemd.unit=postfix.service"
            output:
              logstash:
                enabled: true
                hosts: ["${cfg.publishLogs.destination}:5044"]

            logging.to_files: false
          '';
        };
        logstash = mkIf cfg.process.enable {
          enable = true;
          # logLevel = "info";
          inputConfig = ''
            beats {
               port => 5044
            }
          '';
          filterConfig = ''
            if [@metadata][beat] == "journalbeat" {
              mutate {
                add_field => { "program" => "%{[syslog][identifier]}"}
              }
            }
            ${optionalString cfg.process.postfix.enable postfixFilter}
          '';
          outputConfig = ''
            ${optionalString cfg.process.postfix.enable
              (postfixOut cfg.process.postfix.storeDestination
                cfg.process.postfix.storeDBName)}
          '';
        };
        influxdb = mkIf cfg.store.enable {
          enable = true;
        };
        grafana = mkIf cfg.show.enable {
          enable = true;
          domain = getDefaultDomain cfg.show.domain;
          rootUrl = getDefaultRoot cfg.show.rootURL;
        };
      };
      environment.systemPackages = mkIf cfg.store.enable [
        pkgs.influxdb
      ];
    };
  }
