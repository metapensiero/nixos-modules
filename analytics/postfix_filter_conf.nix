# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- logstash filter configuration for postfix
# :Created:   dom 21 apr 2019 23:20:15 CEST
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2019 Alberto Berti
#

/*
  This configuration and the related patterns file has been taken
  from the following project:
    https://github.com/whyscream/postfix-grok-patterns
  The first extraction was done on commit
  d9a8fc56de1914b60ab0718db9bc752d27963485 of 2019-03-10
*/

let
  patternsFile = ./postfix.grok;
  patternsDir = builtins.dirOf patternsFile;
in ''
  if [program] =~ /^postfix.*\/qmgr$/ {
    grok {
      patterns_dir   => "${patternsDir}"
      patterns_files_glob => "*.grok"
      match          => [ "message", "^%{POSTFIX_QMGR}$" ]
      tag_on_failure => [ "_grok_postfix_qmgr_nomatch" ]
      add_tag        => [ "_grok_postfix_success" ]
    }
  } else if [program] =~ /^postfix.*\/smtp$/ {
    grok {
      patterns_dir   => "${patternsDir}"
      patterns_files_glob => "*.grok"
      match          => [ "message", "^%{POSTFIX_SMTP}$" ]
      tag_on_failure => [ "_grok_postfix_smtp_nomatch" ]
      add_tag        => [ "_grok_postfix_success" ]
    }
  }
  # process key-value data if it exists
  if [postfix_keyvalue_data] {
    kv {
      source       => "postfix_keyvalue_data"
      trim_value   => "<>,"
      prefix       => "postfix_"
      remove_field => [ "postfix_keyvalue_data" ]
    }

    # some post processing of key-value data
    if [postfix_client] {
      grok {
        patterns_dir   => "${patternsDir}"
        patterns_files_glob => "*.grok"
        match          => ["postfix_client", "^%{POSTFIX_CLIENT_INFO}$"]
        tag_on_failure => [ "_grok_kv_postfix_client_nomatch" ]
        remove_field   => [ "postfix_client" ]
      }
    }
    if [postfix_relay] {
      grok {
        patterns_dir   => "${patternsDir}"
        patterns_files_glob => "*.grok"
        match          => ["postfix_relay", "^%{POSTFIX_RELAY_INFO}$"]
        tag_on_failure => [ "_grok_kv_postfix_relay_nomatch" ]
        remove_field   => [ "postfix_relay" ]
      }
    }
    if [postfix_delays] {
      grok {
        patterns_dir   => "${patternsDir}"
        patterns_files_glob => "*.grok"
        match          => ["postfix_delays", "^%{POSTFIX_DELAYS}$"]
        tag_on_failure => [ "_grok_kv_postfix_delays_nomatch" ]
        remove_field   => [ "postfix_delays" ]
      }
    }
  }

  # process command counter data if it exists
  if [postfix_command_counter_data] {
    grok {
      patterns_dir   => "${patternsDir}"
      patterns_files_glob => "*.grok"
      match          => ["postfix_command_counter_data", "^%{POSTFIX_COMMAND_COUNTER_DATA}$"]
      tag_on_failure => ["_grok_postfix_command_counter_data_nomatch"]
      remove_field   => ["postfix_command_counter_data"]
    }
  }

  # Do some data type conversions
  mutate {
    convert => [
      # list of integer fields
      "postfix_anvil_cache_size", "integer",
      "postfix_anvil_conn_count", "integer",
      "postfix_anvil_conn_rate", "integer",
      "postfix_client_port", "integer",
      "postfix_cmd_auth", "integer",
      "postfix_cmd_auth_accepted", "integer",
      "postfix_cmd_count", "integer",
      "postfix_cmd_count_accepted", "integer",
      "postfix_cmd_data", "integer",
      "postfix_cmd_data_accepted", "integer",
      "postfix_cmd_ehlo", "integer",
      "postfix_cmd_ehlo_accepted", "integer",
      "postfix_cmd_helo", "integer",
      "postfix_cmd_helo_accepted", "integer",
      "postfix_cmd_mail", "integer",
      "postfix_cmd_mail_accepted", "integer",
      "postfix_cmd_quit", "integer",
      "postfix_cmd_quit_accepted", "integer",
      "postfix_cmd_rcpt", "integer",
      "postfix_cmd_rcpt_accepted", "integer",
      "postfix_cmd_rset", "integer",
      "postfix_cmd_rset_accepted", "integer",
      "postfix_cmd_starttls", "integer",
      "postfix_cmd_starttls_accepted", "integer",
      "postfix_cmd_unknown", "integer",
      "postfix_cmd_unknown_accepted", "integer",
      "postfix_nrcpt", "integer",
      "postfix_postscreen_cache_dropped", "integer",
      "postfix_postscreen_cache_retained", "integer",
      "postfix_postscreen_dnsbl_rank", "integer",
      "postfix_relay_port", "integer",
      "postfix_server_port", "integer",
      "postfix_size", "integer",
      "postfix_status_code", "integer",
      "postfix_termination_signal", "integer",

      # list of float fields
      "postfix_delay", "float",
      "postfix_delay_before_qmgr", "float",
      "postfix_delay_conn_setup", "float",
      "postfix_delay_in_qmgr", "float",
      "postfix_delay_transmission", "float",
      "postfix_postscreen_violation_time", "float"
    ]
  }
''
