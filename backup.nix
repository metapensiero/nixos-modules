# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- backup system
# :Created:   dom 03 feb 2019 16:16:00 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2019 Alberto Berti
#

{config, pkgs, lib, ...}:
  with lib;
  let
    bckCfg = config.metapensiero.backup;
    /* Creates an environment (i.e. a package that links resource of
       other packages) with scripts for helping the user in dealing
        with the backup
    */
    bckEnv = pkgs.buildEnv {
      name = "backup-scripts";
      paths = [
        (mountBackupScript bckCfg)
      ];
    };
    bckEnvFName = "backup_mount_env";
    /* Given a backup configuration genereates an helper script for
       mounting and using the backup
    */
    mountBackupScript = {mountDir, options, keyFile, ...}:
      with pkgs;
      writeShellScriptBin "mount-backup-fs" ''
        set -e
        PASSWD=$(<${bckCfg.keyFile})
        USER=${bckCfg.options.user}
        export PASSWD USER
        if [[ -z $1 ]]; then
          echo "Usage: \"$0 <mount-point> [<relative path to repo>>]\" "
          exit 0
        fi
        ${cifs-utils}/bin/mount.cifs ${bckCfg.options.service} $1 \
          && echo "Mounted backup service '${bckCfg.options.service}' on '$1'"
        if [[ -n $2 ]]; then
          export BORG_REPO="$1$2"
        fi
        echo -e "Access your backup with \"borg list <repo>[::<archive>]\" and \"borg extract\" commands."
        echo "See the page at https://borgbackup.readthedocs.io/en/stable/usage/general.html for help."
        echo "Type C-d when finished."
        export BORG_PASSPHRASE=$PASSWD
        bash --rcfile <(echo 'PS1="\n\[\033[1;31m\][\u@\h:\w] backup\$\[\033[0m\] "') -i
        umount $1
      '';
      systemBackupName = "system";
      systemBorgJobName = "borgbackup-job-${systemBackupName}";
      systemBorgJobService = "${systemBorgJobName}.service";
      systemRepoName = bckCfg.mountDir + "/${systemBackupName}";
  in {
    /* Backup configuration definition */
    options.metapensiero = {
      backup = {
        enable = mkEnableOption "metapensiero backup";
        mountDir = mkOption {
          description = ''
            Backup filesystem mount directory
          '';
          default = "/mnt/backup";
          type = with types; path;
        };
        mountRequiredBy = mkOption {
          description = ''
            A list of unit names that will need to depend on the mount unit to
            work i.e. a list of all the backup jobs configured using this module.
          '';
          default = [];
          type = with types; listOf str;
        };
        keyDestDir = mkOption {
          description = ''
            Directory used to store files containing secrets to pass to systemd.
          '';
          default = "/opt/keys";
          type = with types; path;
        };
        keyFile = mkOption {
          description = ''
            File containing the security token (like a password or cert) for the
            backup operations. It will be used to mount the backup partition
            and as borg backup password.
          '';
          default = null;
          type = with types; path;
        };
        options = mkOption {
          description = ''
            A set of options to be used by the backup system.
          '';
          default = {};
          type = with types; attrs;
        };
        system = {
          enable = mkEnableOption "metapensiero system backup";
          includePaths = mkOption {
            description = ''
              List of paths included in the system backup.
            '';
            default = [ "/var/lib" ];
            type = with types; listOf path;
          };
          excludePaths = mkOption {
            description = ''
              List of paths excluded from backup.
            '';
            default = [];
            type = with types; listOf path;
          };
          backupPreScript = mkOption {
            description = ''
              The script to execute before the backup.
            '';
            default = null;
            type = with types; nullOr str;
          };
          backupPostScript = mkOption {
            description = ''
              The script to execute after the backup.
            '';
            default = null;
            type = with types; nullOr str;
          };
          startAt = mkOption {
            type = with types; either str (listOf str);
            default = "daily";
            description = ''
              When or how often the backup should run.
              Must be in the format described in
              <citerefentry><refentrytitle>systemd.time</refentrytitle>
              <manvolnum>7</manvolnum></citerefentry>.
              If you do not want the backup to start
              automatically, use <literal>[ ]</literal>.
            '';
          };
        };
      };
    };
    /* Configuration setup */
    config.environment.systemPackages = mkIf bckCfg.enable [ bckEnv ];
    config.boot.supportedFilesystems = mkIf bckCfg.enable ["cifs"];
    /* Generate a config file for the .mount unit containing the
       "secrets" required to mount the external fs
    */
    config.system.activationScripts = mkIf bckCfg.enable {
      metapensieroBackup = ''
        mkdir -p ${bckCfg.mountDir}
        mkdir -p ${bckCfg.keyDestDir} && chown root:root ${bckCfg.keyDestDir} \
                 && chmod 750 ${bckCfg.keyDestDir}
        echo "PASSWD=$(<${bckCfg.keyFile})" > ${bckCfg.keyDestDir}/${bckEnvFName}
        echo "USER=${bckCfg.options.user}" >> ${bckCfg.keyDestDir}/${bckEnvFName} \
             && chmod 750 ${bckCfg.keyDestDir}/${bckEnvFName}
      '';
    };
    /* Install a .mount unit that will be required by all the backup
       jobs and that takes care of mounting the external fs and
       unmounting it as soon as it's not required anymore.
    */
    config.systemd.mounts = mkIf bckCfg.enable [{
      before = bckCfg.mountRequiredBy;
      description = "Mount backup filesystem";
      mountConfig = {
        EnvironmentFile = "${bckCfg.keyDestDir}/${bckEnvFName}";
      };
      options = "noauto,rw";
      requiredBy = bckCfg.mountRequiredBy;
      type = "cifs";
      unitConfig = {
        StopWhenUnneeded = true;
      };
      what = bckCfg.options.service;
      where = bckCfg.mountDir;
    }];
    /* System backup configuration */
    config.metapensiero.backup.enable = mkIf bckCfg.system.enable true;
    config.metapensiero.backup.mountRequiredBy = mkIf bckCfg.system.enable
      [ systemBorgJobService ];
    config.services.borgbackup.jobs = mkIf bckCfg.system.enable {
      ${systemBackupName} = {
        archiveBaseName = "${config.networking.hostName}-site-${systemBackupName}";
        compression = "zstd";
        doInit = true;
        encryption = {
          # see https://borgbackup.readthedocs.io/en/stable/usage/init.html#encryption-modes
          mode = "repokey-blake2";
          passCommand = ''
            cat ${bckCfg.keyFile}
          '';
        };
        extraCreateArgs = "--stats";
        paths = bckCfg.system.includePaths;
        exclude = bckCfg.system.excludePaths;
        postHook = ''
          ${optionalString (bckCfg.system.backupPostScript != null)
            bckCfg.system.backupPostScript
          }
        '';
        preHook = ''
          echo "Borg repo is: $BORG_REPO"
          ${optionalString (bckCfg.system.backupPreScript != null)
            bckCfg.system.backupPreScript
          }
          export BORG_RELOCATED_REPO_ACCESS_IS_OK="yes"
        '';
        prune = {
          keep = {
            within = "1d"; # Keep all archives from the last day
            daily = 7;
            weekly = 4;
            monthly = -1;  # Keep at least one archive for each month
          };
        };
        repo = systemRepoName;
        startAt = bckCfg.system.startAt;
      };
    };
    config.systemd.services = mkIf bckCfg.system.enable {
      "metapensiero-backup-prepare-${systemBackupName}" = {
        description = "Backup prepare job for system backup";
        requiredBy = [ systemBorgJobService ];
        before = [ systemBorgJobService ];
        script = ''
          echo "metapensiero-backup-prepare-${systemBackupName} runs"
          mkdir -p ${systemRepoName}
        '';
        serviceConfig = {
          Type = "oneshot";
        };
        unitConfig = {
          RequiresMountsFor = "${bckCfg.mountDir}";
        };
        # wantedBy = [
        #   "multi-user.target"
        # ];
      };
    };
    config.systemd.timers = mkIf bckCfg.system.enable {
      ${systemBorgJobName} = {
        timerConfig = {
          RandomizedDelaySec = "2h";
        };
      };
    };
  }
