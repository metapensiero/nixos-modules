# -*- coding: utf-8 -*-
# :Project:   metapensiero-nixos-modules -- global configuration
# :Created:   lun 06 gen 2020 17:01:31 CET
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2020 Alberto Berti
#

{config, pkgs, lib, ...}:
  with lib;
  let
    cfg = config.metapensiero;
    orphu = "orphu.arstecnica.it";
  in {
    options.metapensiero = {
      lib = mkOption {
        type = types.attrs;
        default = import ./lib {inherit config lib pkgs;};
      };
      monitorHost = mkOption {
        type = types.str;
        default = orphu;
        description = "Monitor host";
      };
    };
  }
